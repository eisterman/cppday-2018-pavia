from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

setup(
    name="typvarpurecy",
    ext_modules = cythonize([Extension("typvarpurecy",["typ-var-pure-cy.pyx"])])
)
