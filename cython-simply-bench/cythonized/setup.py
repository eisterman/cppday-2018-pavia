from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

setup(
    name="typvarcython",
    ext_modules = cythonize([Extension("typvarcython",["typ-var-cython.pyx"])])
)
