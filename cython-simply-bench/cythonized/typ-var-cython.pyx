cdef f(double x):
    return x ** 2 - x


cpdef integrate_f(double a, double b, long N):
    cdef:
        double s = 0
        double dx = (b - a) / N
    for i in range(N):
        s += f(a + i * dx)
    return s * dx

